namespace backend.Application.DependencyInjection;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddServices(this IServiceCollection services)
    {
        services.AddAutoMapper(typeof(AppContext).Assembly);
        services.AddScoped<ICountryService, CountryService>();
        return services;
    }
}