using backend.Domain;

namespace backend.Application;

public class CountryService : ICountryService
{
    private ICountryRepository repository;

    public CountryService(ICountryRepository repository)
    {
        this.repository = repository;
    }

    public Country CreateCountry(Country country)
    {
        return this.repository.Create(country);
    }
    
    public Country DeleteCountry(Guid id)
    {
        return this.repository.Delete(id);
    }
    
    public bool CountryExists(Guid id)
    {
        return this.repository.Exists(id);
    }
    

    public Country UpdateCountry(Guid id, Country country)
    {
        Country updated = this.repository.GetById(id);
        updated.Name = country.Name;
        updated.Capital = country.Capital;
        updated.Population = country.Population;
        return this.repository.Update(updated);
    }

    public IEnumerable<Country> GetAllCountries()
    {
        return this.repository.GetAll();
    }

    public Country GetCountryById(Guid id)
    {
        return this.repository.GetById(id);
    }
}