using AutoMapper;
using backend.Domain;

namespace backend.Infrastructure;

public class DbCountryProfile : Profile
{
    public DbCountryProfile()
    {
        this.CreateMap<DbCountry, Country>().ReverseMap();
    }
}