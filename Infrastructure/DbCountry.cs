namespace backend.Infrastructure;

public class DbCountry : DbEntity
{
    public string Name { get; set; }
    public string Capital { get; set; }
    public int Population { get; set; }
    
    public DbCountry(Guid id) : base(id)
    {
    }
}