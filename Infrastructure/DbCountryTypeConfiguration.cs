using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace backend.Infrastructure;

public class DbCountryTypeConfiguration : IEntityTypeConfiguration<DbCountry>
{
    public void Configure(EntityTypeBuilder<DbCountry> builder)
    {
        builder.ToTable("Countries");
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Name).IsRequired();
        builder.Property(x => x.Capital).IsRequired();
        builder.Property(x => x.Population).IsRequired();
    }
}