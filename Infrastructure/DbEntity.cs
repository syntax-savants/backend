namespace backend.Infrastructure;

public abstract class DbEntity
{
    public DbEntity(Guid id)
    {
        this.Id = id;
    }

    public Guid Id { get; set; }
}
