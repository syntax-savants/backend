# Backend

## Run Project
```bash
# Create db service
docker-compose up -d

# Run project
dotnet run
```
## Run Examples
Go to swagger and use the api rest: https://localhost:7258/swagger/index.html

## Add Migrations
```bash
dotnet ef migrations add <Name>
```