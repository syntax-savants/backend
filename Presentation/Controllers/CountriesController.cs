using backend.Application;
using backend.Domain;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("/api/v1/countries")]
public class CountriesController : ControllerBase
{
    private ICountryService service;

    public CountriesController(ICountryService service)
    {
        this.service = service ?? throw new ArgumentNullException(nameof(service));
    }

    [HttpGet]
    public IActionResult GetAllCountries()
    {
        return Ok(this.service.GetAllCountries());
    }
    
    [HttpGet("{id}")]
    public IActionResult GetCountryById(Guid id)
    {
        if (!this.service.CountryExists(id))
        {
            return NotFound();
        }
        
        return Ok(this.service.GetCountryById(id));
    }
    
    [HttpDelete("{id}")]
    public IActionResult DeleteCountry(Guid id)
    {
        if (!this.service.CountryExists(id))
        {
            return NotFound();
        }
        this.service.DeleteCountry(id);
        return NoContent();
    }
    
    [HttpPost]
    public IActionResult CreateCountry([FromBody] Country country)
    {
        var newCountry = this.service.CreateCountry(country);
        return Ok(new { id = newCountry.Id });
    } 
    
    [HttpPut("{id}")]
    public IActionResult UpdateCountry(Guid id, [FromBody] Country country)
    {
        if (!this.service.CountryExists(id))
        {
            return NotFound();
        }
        
        this.service.UpdateCountry(id, country);
        return NoContent();
    }
}