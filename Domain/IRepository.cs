using backend.Domain.models;

namespace backend.Domain;

public interface IRepository<TEntity, TId>
    where TEntity : Entity<TId>
    where TId : notnull
{
    public IEnumerable<TEntity> GetAll();

    public TEntity Create(TEntity entity);

    public bool Exists(TId id);

    public TEntity Delete(TId id);

    public TEntity GetById(TId id);

    public TEntity Update(TEntity entity);
}
