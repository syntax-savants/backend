namespace backend.Domain;

public interface ICountryRepository : IRepository<Country, Guid>
{
    
}